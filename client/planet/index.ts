import client from '../../lib/client';

import rename from './rename';
import detail from './detail';
import { build } from './build';
import { check } from './check';
import { pillow } from './pillow';

client.on('planet', async (ws, obj) => {
  switch (obj.do) {
    case 'rename':
      rename(ws, obj.arr);
      break;

    case 'detail':
      detail(ws, obj.arr);
      break;

    case 'build':
      build(ws, obj.arr);
      break;

    case 'check':
      check(ws, obj.arr);
      break;

    case 'pillow':
      pillow(ws, obj.arr);
      break;

    default:
      break;
  }
});
