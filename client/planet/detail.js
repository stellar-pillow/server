const c = require('../../lib/common');

module.exports = async (ws, arr) => {
  const [index] = arr;
  if (!c.intBetween(index, 0, ws.planets.length)) {
    ws.sendError('validation');
    return;
  }

  const planet = ws.planets[index];

  try {
    ws.sendJSON(planet.pdArray(index));
  } catch (err) {
    console.log(err);
    ws.sendError('Unknown');
  }
};
