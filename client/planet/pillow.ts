import * as c from '../../lib/common';


export const pillow = async (ws, arr): Promise<void> => {
  const [planetI, structI, nbPillow] = arr;

  if (
    !c.intBetween(planetI, 0, ws.planets.length)
    || !c.intBetween(structI, 0, ws.planets[planetI].structs.length)
    || ws.player.pillows < nbPillow
  ) {
    ws.sendError('validation');
    return;
  }

  const planet = ws.planets[planetI];
  const struct = planet.structs[structI];

  if (struct.thread === undefined) {
    ws.sendError('validation');
    return;
  }

  struct.thread.time = struct.thread.time.getTime() - nbPillow * 3600000;
  await planet.save();
  ws.sendJSON(planet.pdArray(planetI));
};
