const c = require('../../lib/common');


module.exports = async (ws, arr) => {
  const [index, name] = arr;

  if (
    !c.intBetween(index, 0, ws.planets.length)
    || name.length > 16
    || name.length < 1
  ) {
    ws.sendError('validation');
    return;
  }

  const planet = ws.planets[index];

  try {
    planet.name = name;
    await planet.save();
    ws.sendJSON(['planet', index, planet.name, planet.type, planet.size]);
  } catch (err) {
    console.log(err);
    ws.sendError('Unknown');
  }
};
