import { structs as sData, StructId as sId } from 'sp-shared';
import { PlanetThread } from './PlanetThread';
import * as c from '../../lib/common';

/**
 * [planetIndex, action, slot, (struct id)]
 */

export const build = async (ws, arr): Promise<void> => {
  const [planetIndex, action, slot] = arr;

  if (!c.intBetween(planetIndex, 0, ws.planets.length) || !c.intBetween(action, 0, 3)) {
    ws.sendError('validation');
    return;
  }

  const planet = ws.planets[planetIndex];

  switch (action) {
    case PlanetThread.Upgrade: {
      if (c.intBetween(slot, 0, planet.structs.length + 1)) {
        // define variables depending new struct or not
        let structId: number;
        let level: number;
        if (
          slot < planet.structs.length
          && planet.structs[slot].thread.id !== PlanetThread.Upgrade
          && planet.structs[slot].thread.id !== PlanetThread.Downgrade
        ) {
          // upgrade
          structId = planet.structs[slot].id;
          level = planet.structs[slot].level;
        } else if (
          c.intBetween(arr[3], 0, sData.length) // valid struct id
          && !(planet.structs.length >= planet.maxSlots) // slot limit
        ) {
          // new struct
          structId = arr[3];
          level = 0;
        } else {
          // validation errors
          ws.sendError('validation');
          return;
        }

        const { pop, metal, crystal } = sData[structId].cost(level + 1);
        if (!planet.hasResources(pop, metal, crystal)) {
          ws.sendError('validation');
          return;
        }

        const { maxPower, power } = planet.loop();
        const powerDiff = sData[structId].power(level + 1) - sData[structId].power(level);
        if (powerDiff + power > maxPower) {
          ws.sendError('validation');
          return;
        }

        planet.pop -= pop;
        planet.metal -= metal;
        planet.crystal -= crystal;

        // create struct if new struct
        if (slot === planet.structs.length) {
          planet.structs.push({ id: structId, level });
        }
        // add upgrade to thread
        planet.structs[slot].thread = {
          id: PlanetThread.Upgrade,
          time: Date.now(),
        };
        // save & send
        await planet.save();
        ws.sendJSON(planet.pdArray(planetIndex));
      } else {
        ws.sendError('validation');
        // return;
      }
      break;
    }

    case PlanetThread.Downgrade: {
      // TODO: fix duplicate code
      if (c.intBetween(slot, 0, planet.structs.length + 1)) {
        // define variables depending new struct or not
        let structId: number;
        let level: number;
        if (
          slot < planet.structs.length
          && planet.structs[slot].thread.id !== PlanetThread.Upgrade
          && planet.structs[slot].thread.id !== PlanetThread.Downgrade
        ) {
          // downgrade
          structId = planet.structs[slot].id;
          level = planet.structs[slot].level;
        } else {
          // validation errors
          ws.sendError('validation');
          return;
        }

        // Check for generator
        if (structId === sId.Generator) {
          const { maxPower, power } = planet.loop();
          const powerDiff = sData[structId].limitBuff(level) - sData[structId].limitBuff(level - 1);
          if (power > maxPower - powerDiff) {
            ws.sendError('validation');
            return;
          }
        }

        const { metal, crystal } = sData[structId].cost(level + 1);

        planet.metal += Math.round(metal * 0.8);
        planet.crystal += Math.round(crystal * 0.8);

        // add downgrade to thread
        planet.structs[slot].thread = {
          id: PlanetThread.Downgrade,
          time: Date.now(),
        };
        // save & send
        await planet.save();
        ws.sendJSON(planet.pdArray(planetIndex));
      } else {
        ws.sendError('validation');
        // return;
      }
      break;
    }
    default:
      break;
  }
};
