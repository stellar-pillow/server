const { planets: pData } = require('sp-shared');

const c = require('../../lib/common');
const Planet = require('../../schema/planet');


function pathGenerator(count) {
  const hubs = ['A', 'B', 'C', 'D', 'E'];
  const arr = [];
  for (let i = 0; i < count; i += 1) {
    const hub = c.randArrElem(hubs);
    hubs.splice(hubs.indexOf(hub), 1);
    arr[i] = { hub, distance: c.randInt(800, 2000) };
  }
  return arr;
}

module.exports = async (ws, arr) => {
  const [type, size] = arr;

  if (
    !c.intBetween(type, 0, pData.types.length)
    || !c.intBetween(size, 0, pData.sizes.length)
    || ws.planets.length >= 2
  ) {
    ws.sendError('validation');
    return;
  }

  const price = Math.round(pData.sizes[size].price * pData.types[type].price);

  if (ws.player.credits < price) {
    ws.sendError('validation');
    return;
  }

  try {
    // credits
    ws.player.credits -= price;
    await ws.player.save();
    // create planet
    const planet = await Planet.create({
      owner: ws.player._id,
      type,
      size,
      paths: pathGenerator(2),
    });
    const index = ws.planets.push(planet) - 1;
    ws.sendJSON(['planet', index, planet.name, planet.type, planet.size]);
  } catch (err) {
    console.log(err);
    ws.sendError('Unknown');
  }
};
