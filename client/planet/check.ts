import { structs as sData, StructId as sId } from 'sp-shared';
import * as c from '../../lib/common';
import { PlanetThread } from './PlanetThread';

export const check = async (ws, arr): Promise<void> => {
  const [planetI, structI] = arr;

  if (
    !c.intBetween(planetI, 0, ws.planets.length)
    || !c.intBetween(structI, 0, ws.planets[planetI].structs.length)
  ) {
    ws.sendError('validation');
    return;
  }

  const planet = ws.planets[planetI];
  const struct = planet.structs[structI];

  switch (struct.thread.id) {
    case PlanetThread.Upgrade:
    case PlanetThread.Downgrade: {
      const { time } = sData[struct.id].cost(struct.level + 1);
      if (struct.thread.time.getTime() + time <= Date.now()) {
        // Inc/dec level if upgrade or downgrade
        const diff = struct.thread.id === PlanetThread.Upgrade ? 1 : -1;
        struct.level += diff;
        struct.thread = undefined;
      } else {
        ws.sendError('validation');
        return;
      }

      // if metal, crystal or shelter => start production
      if (struct.id === sId.Metal || struct.id === sId.Crystal || struct.id === sId.Shelter) {
        struct.thread = { id: PlanetThread.Produce, time: Date.now() };
      }

      // if struct level is 0, remove it
      if (struct.level < 1) {
        planet.structs.splice(structI, 1);
      }

      await planet.save();
      ws.sendJSON(planet.pdArray(planetI));
      break;
    }
    case PlanetThread.Produce: {
      let prodType: 'metal' | 'crystal' | 'pop';
      if (struct.id === sId.Metal) prodType = 'metal';
      else if (struct.id === sId.Crystal) prodType = 'crystal';
      else if (struct.id === sId.Shelter) prodType = 'pop';
      else {
        ws.sendError('validation');
        return;
      }

      const incomePerH = sData[struct.id].incomePerH(struct.level, planet.buffs[prodType]);
      const duration = sData[struct.id].duration(struct.level);

      if (struct.thread.time.getTime() + duration <= Date.now()) {
        planet[prodType] += Math.round(incomePerH * (duration / 3600000));
      } else {
        const hours = (Date.now() - struct.thread.time.getTime()) / 3600000;
        planet[prodType] += Math.floor(incomePerH * hours);
      }

      struct.thread.time = Date.now();
      await planet.save();
      ws.sendJSON(planet.pdArray(planetI));
      break;
    }
    default:
      break;
  }
};
