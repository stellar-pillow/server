export enum PlanetThread {
  Upgrade,
  Downgrade,
  Produce,
}
