const c = require('../../lib/common');
const { Fleet } = require('../../schema');

module.exports = async (ws, arr) => {
  const fleets = await Fleet.find({ owner: ws.player._id });
  const [index] = arr;
  if (!c.intBetween(index, 0, fleets.length)) {
    ws.sendError('validation');
    return;
  }

  const fleet = fleets[index];

  try {
    ws.sendJSON(fleet.detailArray(index));
  } catch (err) {
    console.log(err);
    ws.sendError('Unknown');
  }
};
