const client = require('../../lib/client');

const list = require('./list');
const detail = require('./detail');


client.on('fleet', async (ws, obj) => {
  switch (obj.do) {
    case 'list':
      list(ws, obj.arr);
      break;

    case 'detail':
      detail(ws, obj.arr);
      break;

    default:
      break;
  }
});
