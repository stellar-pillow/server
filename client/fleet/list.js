const { Fleet } = require('../../schema');


module.exports = async (ws) => {
  const fleets = await Fleet.find({ owner: ws.player._id }).populate('planet', 'name');
  const fleetsArr = fleets.map(x => [x.planet.name, x.flagship.id]);
  ws.sendJSON(['fleets', fleetsArr]);
};
