const client = require('../lib/client');
const auth = require('../lib/auth');
const Planet = require('../schema/planet');

const version = 1;

/**
 * Output:
 * Duplicate, Unknown, Invalid, Signed up, Signed in
 */
client.on('auth', async (ws, obj) => {
  if (obj.v !== version) {
    ws.sendError('please update!');
    return;
  }

  if (obj.cpw) {
    const result = auth.changePassword(ws.player, obj.opw, obj.npw);
    if (result instanceof Error) {
      ws.sendError(result.message);
      return;
    }
    ws.sendError('成功，请重新登录');
    return;
  }

  const player = obj.signup
    ? await auth.newPlayer(obj.id, obj.pw, ws.ip)
    : await auth.getPlayer(obj.id, obj.pw, ws.ip);

  if (player instanceof Error) {
    ws.sendError(player.message);
    return;
  }

  ws.player = player;
  ws.planets = await Planet.find({ owner: ws.player._id });

  const planets = [];
  for (let i = 0; i < ws.planets.length; i += 1) {
    planets[i] = [
      ws.planets[i].name,
      ws.planets[i].stars.metal,
      ws.planets[i].stars.crystal,
      ws.planets[i].stars.pop,
      ws.planets[i].stars.size,
    ];
  }

  const reply = ['auth', player.nickname, player.credits, player.pillows, planets];
  ws.sendJSON(reply);
});
