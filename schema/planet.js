const { structs: sData, StructId: sId } = require('sp-shared');

const { mongoose } = require('../lib/mongoose');

const { Schema } = mongoose;
const { ObjectId } = mongoose.Types;

const pathSchema = new Schema({
  hub: String,
  distance: Number,
}, { _id: false });

const structSchema = new Schema({
  id: Number,
  level: Number,
  thread: {
    id: Number,
    time: Date,
  },
  isStation: { type: Boolean, default: false },
}, { _id: false });

const shipSchema = new Schema({
  id: Number,
  amount: Number,
}, { _id: false });

const itemSchema = new Schema({
  type: { type: Number, required: true },
  id: { type: Number, required: true },
  level: Number,
  amount: Number,
}, { _id: false });

const planetSchema = new Schema({
  name: { type: String, default: 'My planet' },
  owner: { type: ObjectId, required: true, ref: 'Player' },
  isHome: { type: Boolean, default: false },
  stars: {
    metal: { type: Number, required: 1 },
    crystal: { type: Number, required: 1 },
    pop: { type: Number, default: 1 },
    size: { type: Number, default: 1 },
  },
  paths: [pathSchema],
  metal: { type: Number, default: 0 },
  crystal: { type: Number, default: 0 },
  pop: { type: Number, default: 5 },
  structs: [structSchema],
  ships: [shipSchema],
  depot: [itemSchema],
});

planetSchema.virtual('buffs').get(function get() {
  return {
    metal: this.stars.metal * 0.5 + 0.5,
    crystal: this.stars.crystal * 0.5 + 0.5,
    pop: this.stars.pop * 0.5 + 0.5,
  };
});

planetSchema.virtual('maxSlots').get(function get() {
  return 4 + 2 * this.stars.size;
});

planetSchema.methods.pdArray = function get(index) {
  const paths = this.paths.map(x => [x.hub, x.distance]);
  const structs = this.structs.map(x => [
    x.id,
    x.level,
    x.thread.id === undefined ? [] : [x.thread.id, x.thread.time.getTime()],
    x.isStation ? 1 : 0,
  ]);
  return [
    'pd',
    index,
    this.metal,
    this.crystal,
    this.pop,
    paths,
    structs,
    this.ships,
  ];
};

planetSchema.methods.loop = function get() {
  let maxPop = 5;
  let maxPower = 0;
  let power = 0;
  for (let i = 0; i < this.structs.length; i += 1) {
    const { id, level } = this.structs[i];
    // ignore level 0
    if (level !== 0) {
      switch (id) {
        case sId.Shelter:
          maxPop += sData[id].limitBuff(level);
          break;

        case sId.Generator:
          maxPower += sData[id].limitBuff(level);
          break;

        default:
          break;
      }
    }
    if (this.structs[i].thread.id === 0) {
      power += sData[id].power(level + 1);
    } else {
      power += sData[id].power(level);
    }
  }
  return { maxPop, maxPower, power };
};

planetSchema.methods.hasResources = function check(pop, metal, crystal) {
  return this.metal >= metal && this.crystal >= crystal && this.pop >= pop;
};

const Planet = mongoose.model('Planet', planetSchema);
module.exports = Planet;
