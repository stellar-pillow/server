const Ip = require('./ip');
const Planet = require('./planet');
const Player = require('./player');
const Fleet = require('./fleet');


module.exports = {
  Ip, Planet, Player, Fleet,
};
