const { mongoose } = require('../lib/mongoose');

const { Schema } = mongoose;
const { ObjectId } = mongoose.Types;

const modSchema = new Schema(
  {
    id: Number,
    level: Number,
  },
  { _id: false },
);

const aircraftSchema = new Schema(
  {
    id: Number,
    amount: Number,
    atkStrategy: Number,
    defStrategy: Number,
  },
  { _id: false },
);

const fleetSchema = new Schema({
  owner: { type: ObjectId, required: true, ref: 'Player' },
  planet: { type: ObjectId, required: true, ref: 'Planet' },
  flagship: {
    id: { type: Number, required: true },
    level: { type: Number, required: true },
    curHealth: { type: Number, required: true },
    atkStrategy: { type: Number, required: true },
    defStrategy: { type: Number, required: true },
  },
  mods: [modSchema],
  aircrafts: [aircraftSchema],
});

fleetSchema.methods.detailArray = function get(index) {
  const mods = this.mods.map(x => [x.id, x.level]);
  const aircrafts = this.aircrafts.map(x => [x.id, x.amount, x.atkStrategy, x.defStrategy]);

  return [
    'fd',
    index,
    this.flagship.id,
    this.flagship.level,
    this.flagship.curHealth,
    this.flagship.atkStrategy,
    this.flagship.defStrategy,
    mods,
    aircrafts,
  ];
};

const Fleet = mongoose.model('Fleet', fleetSchema);
module.exports = Fleet;
