const { mongoose } = require('../lib/mongoose');

const { Schema } = mongoose;

const ipSchema = new Schema({
  ip: { type: String, unique: true },
  signup: {
    count: { type: Number, default: 0 },
    time: { type: Date, default: Date.now },
  },
  badlogin: {
    count: { type: Number, default: 0 },
    time: { type: Date, default: Date.now },
  },
});

const Ip = mongoose.model('Ip', ipSchema);
module.exports = Ip;
