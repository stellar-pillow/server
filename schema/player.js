const { mongoose } = require('../lib/mongoose');

const { Schema } = mongoose;


const playerSchema = new Schema({
  auth: {
    email: {
      value: { type: String, unique: true, sparse: true },
      verified: { type: Boolean, default: false },
    },
    qq: {
      value: { type: String, unique: true, sparse: true },
      verified: { type: Boolean, default: false },
    },
    password: {
      salt: String,
      key: String,
      iterations: Number,
    },
  },
  nickname: { type: String },
  credits: { type: Number, default: 200 },
  pillows: { type: Number, default: 20 },
  stats: {
    timeCreated: { type: Date, default: Date.now },
    lastLogin: Date,
    lastIp: String,
  },
});

const Player = mongoose.model('Player', playerSchema);
module.exports = Player;
