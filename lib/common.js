/**
 * Return true if str is an integer
 * @param {String} str
 */
module.exports.strIsInt = str => /^[1-9]\d*$/.test(str);

/**
 * Return a random element from array
 * @param {Array} arr
 */
module.exports.randArrElem = arr => arr[Math.floor(Math.random() * arr.length)];

/**
 * Return a random integer between min (incl.) and max (excl.)
 * @param {Number} min
 * @param {Number} max
 */
module.exports.randInt = (min, max) => Math.floor(Math.random() * (max - min) + min);

/**
 * Return true if it is an integer between min (incl.) and max (excl.)
 * @param {Number} int
 * @param {Number} min
 * @param {Number} max
 */
module.exports.intBetween = (int, min, max) => Number.isInteger(int) && int >= min && int < max;
