const crypto = require('crypto');

const c = require('./common');
const {
  Player, Ip, Planet, Fleet,
} = require('../schema');

const iterations = 10000;


const auth = {};
const signupDelay = 12 * 60 * 60 * 1000;
const loginDelay = 5 * 60 * 1000;

/**
 * Create a new player
 * @param {String} id
 * @param {String} password
 * @param {String} ip
 */
auth.newPlayer = async (id, password, ip) => {
  // check for IP
  let ipRecord = await Ip.findOne({ ip });
  if (!ipRecord) ipRecord = await Ip.create({ ip });
  if (ipRecord.signup.count > 2) {
    if ((Date.now() - ipRecord.signup.time) < signupDelay) {
      return new Error('Try again later');
    }
    ipRecord.signup.count = 0;
  }

  // build pw
  const salt = crypto.randomBytes(16).toString('base64');
  const key = await crypto.pbkdf2Sync(password, salt, iterations, 64, 'sha512').toString('base64');

  const authObj = c.strIsInt(id) ? { 'qq.value': id } : { 'email.value': id };
  authObj.password = {
    salt,
    key,
    iterations,
  };

  // create player
  try {
    const player = await Player.create({
      auth: authObj,
      stats: {
        timeCreated: Date.now(),
        lastLogin: Date.now(),
        lastIp: ip,
      },
    });

    ipRecord.signup.count += 1;
    await ipRecord.save();

    await Planet.create({
      name: 'home',
      owner: player._id,
      isHome: true,
      metal: 750,
      crystal: 750,
    });

    return player;
  } catch (err) {
    if (err.name === 'MongoError' && err.code === 11000) return new Error('Duplicate');
    return new Error('Unknown');
  }
};


/**
 * Create a new player from QQ
 * @param {String} id
 * @param {String} nickname
 */
auth.newPlayerFromQq = async (id, nickname) => {
  // generate random pw
  const possible = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789';
  let password = '';
  for (let i = 0; i < 8; i += 1) {
    password += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  // build pw
  const salt = crypto.randomBytes(16).toString('base64');
  const key = await crypto.pbkdf2Sync(password, salt, iterations, 64, 'sha512').toString('base64');

  const authObj = {
    qq: {
      value: id,
      verified: true,
    },
    password: {
      salt,
      key,
      iterations,
    },
  };

  // create player
  try {
    const player = await Player.create({
      auth: authObj,
      nickname,
      stats: {
        timeCreated: Date.now(),
        lastLogin: Date.now(),
      },
    });


    const planet = await Planet.create({
      name: 'home',
      owner: player._id,
      type: 0,
      size: 0,
      isHome: true,
      metal: 750,
      crystal: 750,
    });

    await Fleet.create({
      owner: player._id,
      planet: planet._id,
      flagship: 0,
      health: 100,
      aircrafts: [
        { id: 0, amount: 5 },
        { id: 1, amount: 4 },
        { id: 2, amount: 3 },
      ],
    });

    return password;
  } catch (err) {
    if (err.name === 'MongoError' && err.code === 11000) return new Error('Duplicate');
    return new Error('Unknown');
  }
};


async function badlogin(ipRecord) {
  ipRecord.badlogin.count += 1;
  ipRecord.badlogin.time = Date.now();
  await ipRecord.save();
  return new Error('Invalid');
}

/**
 * Get a registered player
 * @param {String} id
 * @param {String} password
 * @param {String} ip
 */
auth.getPlayer = async (id, password, ip) => {
  // check for IP
  let ipRecord = await Ip.findOne({ ip });
  if (!ipRecord) ipRecord = await Ip.create({ ip });
  if (ipRecord.badlogin.count > 4) {
    if ((Date.now() - ipRecord.badlogin.time) < loginDelay) {
      return new Error('Try again later');
    }
    ipRecord.badlogin.count = 4;
  }

  // Determine if it's email or qq
  const field = c.strIsInt(id) ? 'auth.qq.value' : 'auth.email.value';

  // try to get this player
  const player = await Player.findOne({ [field]: id });
  if (!player) return badlogin(ipRecord);

  // match key
  const key = await crypto.pbkdf2Sync(
    password, player.auth.password.salt, player.auth.password.iterations, 64, 'sha512',
  ).toString('base64');
  if (key !== player.auth.password.key) return badlogin(ipRecord);

  // return player
  player.stats.lastLogin = Date.now();
  player.stats.lastIp = ip;
  ipRecord.badlogin.count = 0;
  await Promise.all([player.save(), ipRecord.save()]);
  return player;
};

auth.changePassword = async (player, oldPw, newPw) => {
  const key = await crypto.pbkdf2Sync(
    oldPw, player.auth.password.salt, player.auth.password.iterations, 64, 'sha512',
  ).toString('base64');
  if (key !== player.auth.password.key) return new Error('password not matched');

  // build pw
  const newKey = await crypto.pbkdf2Sync(newPw, player.auth.password.salt, iterations, 64, 'sha512').toString('base64');

  player.auth.password.key = newKey;
  player.auth.password.iterations = iterations;
  await player.save();
  return true;
};

module.exports = auth;
