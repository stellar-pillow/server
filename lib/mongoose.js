const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/pillow', { useNewUrlParser: true });
mongoose.set('useCreateIndex', true);

module.exports = {
  mongoose,
};
