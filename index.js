const WebSocket = require('ws');
const client = require('./lib/client');
require('./client/auth');
require('./client/planet');
require('./client/fleet');

const wss = new WebSocket.Server({
  port: 8080,
  // verifyClient: (info) => {
  //   if (!info.secure) return false;
  //   return true;
  // },
});

wss.on('connection', (ws, req) => {
  if (req.headers['x-forwarded-for']) {
    ws.ip = req.headers['x-forwarded-for'].split(/\s*,\s*/)[0];
  } else {
    ws.ip = req.connection.remoteAddress;
  }

  ws.isAlive = 0;

  ws.sendJSON = (string) => {
    ws.send(JSON.stringify(string));
  };

  ws.sendError = (code) => {
    ws.sendJSON(['err', code]);
    ws.terminate();
  };

  ws.on('pong', () => {
    if (ws.player) ws.isAlive = 0;
  });

  ws.on('message', (message) => {
    if (ws.player) {
      console.log(ws.player.nickname + message);
    } else {
      console.log(message);
    }
    let obj;
    try {
      obj = JSON.parse(message);
    } catch (e) {
      ws.terminate();
      return;
    }
    if (!obj.type) ws.terminate();

    client.emit(obj.type, ws, obj);
  });
});

// heartbeat
setInterval(() => {
  wss.clients.forEach((ws) => {
    if (ws.isAlive > 1) {
      ws.terminate();
    } else {
      ws.isAlive += 1;
      ws.ping();
    }
  });
}, 15000);


console.log('Server started');
